<?php
/**
 * Created by PhpStorm.
 * User: wsalazar
 * Date: 3/28/15
 * Time: 10:28 PM
 */

namespace Asseter\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseRegistration;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationController extends BaseRegistration
{
    public function registerAction()
    {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');
        $process = $formHandler->process($confirmationEnabled);
        if ($process) {
            $user = $form->getData();
            $this->container->get('logger')->info(sprintf('New user registration: %s', $user));
            $route = 'fos_user_registration_register';
            $userEmailService = $this->container->get('asseter_user.register.email');
            $mailer = $this->container->get('mailer');
            $userFields = $userEmailService->prepareFields($user, $this->container);
            $body = $this->container->get('templating')->render('AsseterUserBundle:User:emailUserVerificationCode.html.twig', array('content' => $userFields['body']));
            $userEmailService->sendVerificationEmailToUser($userFields, $mailer, $body);
            $flashMessage = 'User for ' . $user->getFullname() . ' ('. $user->getUsername() . ') created successfully';
            $this->setFlash('notice', $flashMessage);
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            return $response;
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
            'form' => $form->createView(),
        ));
    }
}